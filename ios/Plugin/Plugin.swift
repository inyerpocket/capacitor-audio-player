import Foundation
import Capacitor
import AVFoundation
import MediaPlayer

/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitorjs.com/docs/plugins/ios
 */

typealias JSObject = [String:Any]
typealias JSArray = [JSObject]


@objc(AudioPlayerPlugin)
public class AudioPlayerPlugin: CAPPlugin {
    //Main player variables
    var player = AVPlayer()
    var playerItem: AVPlayerItem!
    var playerAsset: AVAsset!
    var _playerItemStatusObserver: NSKeyValueObservation!
    var _playerItemTimeObserver: NSKeyValueObservation!
    var _playerEndObserver: Any!
    var _playerInteruptionObserver: Any!

    var nowPlayingInfo = [String : Any]()
    var initialised: Bool = false
    var commandCenterInitialised: Bool = false
    var playlist = [JSObject?]()
    var currentIndex: Int = 0
    var autoPlaylist = [JSObject?]()
    var autoPlaylistIndex: Int = 0
    var useAutoPlaylist:Bool = true
    var currentState:String = "unknown"
    var currentPosition:Float = 0
    var currentDuration:Float = 0
    var isStream:Bool = false
    var hasError:Bool = false
    var hasLoaded:Bool = false
    var currentVolume:Float = 1
    var isMuted:Bool = false
    var statTime:Float = 30 //seconds
    var statsEnabled:Bool = true

    var timer:Timer?
    let audioSession = AVAudioSession.sharedInstance()

    var previousPlayerTime:Float = -1
    var totalPlaybackTime:Float = 0
    var requiresPlayStat:Bool = false;
    var requiresAutoPlayStat:Bool = false
    var isSeeking:Bool = false

    /**
    *
    * Player utilities
    * These are utility functions
    *
    *
    */

    func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {

        let cgimage = image.cgImage!
        let contextImage: UIImage = UIImage(cgImage: cgimage)
        let contextSize: CGSize = contextImage.size
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        var cgwidth: CGFloat = CGFloat(width)
        var cgheight: CGFloat = CGFloat(height)

        // See what size is longer and create the center off of that
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            cgwidth = contextSize.height
            cgheight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            cgwidth = contextSize.width
            cgheight = contextSize.width
        }

        let rect: CGRect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)

        // Create bitmap image from context using the rect
        let imageRef: CGImage = cgimage.cropping(to: rect)!

        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)

        return image
    }

    private func postStat(url: String) {
        // Prepare URL
        let url = URL(string: url)
        guard let requestUrl = url else { return }
        // Prepare URL Request Object
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "POST"

        // HTTP Request Parameters which will be sent in HTTP Request Body
        let postString = "";
        // Set HTTP Request Body
        request.httpBody = postString.data(using: String.Encoding.utf8);
        // Perform HTTP Request
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in

                // Check for Error
                if let error = error {
                    print("Error took place \(error)")
                    return
                }

                // Convert HTTP Response Data to a String
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    print("Response data string:\n \(dataString)")
                }
        }
        task.resume()
    }

    private func sendSyncEvent(type: Int) {
        var syncData = [String: Any]()

        syncData["type"] = type

        switch type {
        case AudioPlayerStatus.GENERAL:
            syncData["initialised"] = initialised
            syncData["currentIndex"] = currentIndex
            syncData["autoPlaylistIndex"] = autoPlaylistIndex
            syncData["useAutoPlaylist"] = useAutoPlaylist
            syncData["currentState"] = currentState
            syncData["currentPosition"] = currentPosition
            syncData["currentDuration"] = currentDuration
            syncData["isStream"] = isStream
            syncData["hasError"] = hasError
            syncData["hasLoaded"] = hasLoaded
            syncData["currentVolume"] = currentVolume
            syncData["isMuted"] = isMuted
            syncData["statTime"] = statTime
            syncData["statsEnabled"] = statsEnabled

        case AudioPlayerStatus.TRACK:
            syncData["currentIndex"] = currentIndex
            syncData["autoPlaylistIndex"] = autoPlaylistIndex
            syncData["currentState"] = currentState
            syncData["currentPosition"] = currentPosition
            syncData["currentDuration"] = currentDuration
            syncData["isStream"] = isStream
            syncData["hasError"] = hasError
            syncData["hasLoaded"] = hasLoaded

        case AudioPlayerStatus.POSITION:
            syncData["currentIndex"] = currentIndex
            syncData["autoPlaylistIndex"] = autoPlaylistIndex
            syncData["currentState"] = currentState
            syncData["currentPosition"] = currentPosition
            syncData["currentDuration"] = currentDuration

        case AudioPlayerStatus.PLAYLIST:
            syncData["playlist"] = playlist
            syncData["currentIndex"] = currentIndex

        case AudioPlayerStatus.PLAYLIST_AUTO:
            syncData["autoPlaylist"] = autoPlaylist
            syncData["autoPlaylistIndex"] = autoPlaylistIndex

        case AudioPlayerStatus.PLAYLIST_TRACK_CHANGE:
            syncData["currentIndex"] = currentIndex
            syncData["autoPlaylistIndex"] = autoPlaylistIndex

        case AudioPlayerStatus.PLAYLIST_ADD_ITEM:
            syncData["item"] = playlist.last!

        default:
            syncData = [String: Any]()
        }

        self.notifyListeners("sync", data: syncData)
    }

    private func getAutoPlayItem(index:Int) -> JSObject? {
        if !useAutoPlaylist || autoPlaylist.isEmpty {
            return nil
        }

        autoPlaylistIndex = index
        if autoPlaylist[autoPlaylistIndex] == nil {
            autoPlaylistIndex = 0
        }

        let nextItem = autoPlaylist[autoPlaylistIndex]
        autoPlaylistIndex += 1

        if autoPlaylistIndex > (autoPlaylist.count - 1) {
            autoPlaylistIndex = 0
        }

        return nextItem;
    }

    private func resetPlayer() {
        NSLog("Reset Player")

        //cancel timer here
        stopTimer()

        //stop player audio here
//        if let player = player {
            player.pause()
//        }

        currentState = "ready"
        currentPosition = 0
        currentDuration = 0
        isStream = false
        hasError = false
        hasLoaded = false

        previousPlayerTime = -1
        totalPlaybackTime = 0
        requiresPlayStat = false;
        requiresAutoPlayStat = false
        isSeeking = false

        //send sync event
        sendSyncEvent(type: AudioPlayerStatus.GENERAL)
    }

    private func positionUpdate() {
        if player.timeControlStatus == AVPlayer.TimeControlStatus.playing {
            print("Update Pos - Playing")

            if !isStream {
                if isSeeking {
                    previousPlayerTime = -1
                    isSeeking = false
                }
                currentPosition = Float(player.currentTime().seconds)
                currentDuration = Float(player.currentItem?.duration.seconds ?? 0)

                if previousPlayerTime > -1 {
                    totalPlaybackTime = totalPlaybackTime + (currentPosition - previousPlayerTime);
                    print("TOTAL PLAY TIME - \(totalPlaybackTime)");
                }

                if (requiresPlayStat || requiresAutoPlayStat) && totalPlaybackTime >= statTime {
                    if requiresPlayStat && playlist[currentIndex]?["playStatUrl"] != nil {
                        print("LOG PLAY STAT - \(playlist[currentIndex]?["playStatUrl"] ?? "")")
                        postStat(url: playlist[currentIndex]!["playStatUrl"] as! String)
                    } else if requiresAutoPlayStat && playlist[currentIndex]?["autoPlayStatUrl"] != nil {
                        print("LOG AUTO PLAY STAT - \(playlist[currentIndex]?["autoPlayStatUrl"] ?? "")")
                        postStat(url: playlist[currentIndex]!["autoPlayStatUrl"] as! String)
                    }

                    requiresPlayStat = false;
                    requiresAutoPlayStat = false;
                }

                previousPlayerTime = currentPosition;
            } else {
                currentPosition = Float(0)
                currentDuration = Float(0)
            }

            currentVolume = player.volume;
            currentState = "playing"

            //send sync event
            updateNowPlayingInfoCenter()
            sendSyncEvent(type: AudioPlayerStatus.GENERAL)
        }
    }

    @objc func trackDidEnd(_ notification: Notification) {
        print("DID END!!!!!!")
        guard let playerItem = notification.object as? AVPlayerItem, let urlAsset = playerItem.asset as? AVURLAsset else { return }
        print("Sender urlAsset: \(urlAsset.url.absoluteString)")

        playerSkipForward()
    }

    @objc func interruptionHandler(notification: Notification) {
        guard let userInfo = notification.userInfo, let typeValue = userInfo[AVAudioSessionInterruptionTypeKey] as? UInt, let type = AVAudioSession.InterruptionType(rawValue: typeValue) else { return }

        switch type {
            case .began:
                self.playerInteruptionPause()
            case .ended:
                print("Audio Session Interruption Ended")

            default: ()
        }
    }

    private func startTimer() {
        guard timer == nil else { return }
        NSLog("Can Start Timer")
        self.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { [weak self] timer in
            self?.positionUpdate()
        })
    }

    private func stopTimer() {
        guard timer != nil else { return }
        NSLog("Can Stop Timer")
        timer?.invalidate()
        timer = nil
    }

    private func loadPlayerItem(isAuto:Bool) {
        previousPlayerTime = -1;
        totalPlaybackTime = 0;
        hasLoaded = false
        hasError = false

        guard let trackItem = self.playlist[self.currentIndex] else { return }
        guard let pathUrl = URL.init(string: trackItem["assetUrl"] as! String) else { return }
        requiresPlayStat = !isAuto;
        requiresAutoPlayStat = isAuto;

        playerAsset = AVAsset.init(url: pathUrl)
        playerItem = AVPlayerItem.init(asset: playerAsset)

        if trackItem["isStream"] != nil && trackItem["isStream"] as! Bool {
            player.automaticallyWaitsToMinimizeStalling = true
            isStream = true
        } else {
            isStream = false
            player.automaticallyWaitsToMinimizeStalling = false
            playerItem.preferredForwardBufferDuration = 5
        }

        player.replaceCurrentItem(with: playerItem)

        DispatchQueue.main.async {
//            if self._playerItemStatusObserver != nil {
//                self._playerItemTimeObserver?.invalidate()
//            }

            self._playerItemStatusObserver = self.playerItem.observe(\.status) { [weak self] object, change in
                self?.notifyStatus()
            }

            self.startTimer()

            self.setupNowPlayingInfoCenter()
        }

        setNowPlayingInfoCenter()

        //send sync event
        sendSyncEvent(type: AudioPlayerStatus.GENERAL)
    };

    private func notifyStatus() {
        print("player item - status changed")
        //Switch over the status
        let status = playerItem.status

        switch status {
            case .readyToPlay:
                print("player item - ready to play")
                currentState = "loaded"
                if player.timeControlStatus == AVPlayer.TimeControlStatus.playing {
                    currentState = "playing"
                }
                hasLoaded = true
            case .failed:
                print("player item - failed to load")
                hasError = true
                currentState = "error"
            case .unknown:
                print("player item - unknown")
            @unknown default:
                print("player item - unknown2")
        }

        updateNowPlayingInfoCenter()
        sendSyncEvent(type: AudioPlayerStatus.GENERAL)
    }

    private func setupNowPlayingInfoCenter() {
        let commandCenter = MPRemoteCommandCenter.shared()


        commandCenter.playCommand.isEnabled = true

        if isStream {
            commandCenter.pauseCommand.isEnabled = false
            commandCenter.stopCommand.isEnabled = true
        } else {
            commandCenter.pauseCommand.isEnabled = true
            commandCenter.stopCommand.isEnabled = false
        }

        if playlist.indices.contains(currentIndex + 1) || (useAutoPlaylist && !autoPlaylist.isEmpty) {
            commandCenter.nextTrackCommand.isEnabled = true
        } else {
            commandCenter.nextTrackCommand.isEnabled = false
        }

        if playlist.indices.contains(currentIndex - 1) {
            commandCenter.previousTrackCommand.isEnabled = true
        } else {
            commandCenter.previousTrackCommand.isEnabled = false
        }

        commandCenter.changePlaybackPositionCommand.isEnabled = true

        if !commandCenterInitialised {
            commandCenter.togglePlayPauseCommand.isEnabled = false
            commandCenter.seekForwardCommand.isEnabled = false
            commandCenter.seekBackwardCommand.isEnabled = false
            commandCenter.skipForwardCommand.isEnabled = false
            commandCenter.skipBackwardCommand.isEnabled = false

            commandCenter.playCommand.addTarget { [unowned self] event in
                self.playerPlay()
                return .success
            }

            commandCenter.pauseCommand.addTarget { [unowned self] event in
                self.playerPause()
                return .success
            }

            commandCenter.stopCommand.addTarget { [unowned self] event in
                self.playerPause()
                return .success
            }

            commandCenter.changePlaybackPositionCommand.addTarget { [weak self](remoteEvent) -> MPRemoteCommandHandlerStatus in
                guard let self = self else {return .commandFailed}
                if let event = remoteEvent as? MPChangePlaybackPositionCommandEvent {
                    self.playerSeekTo(position: event.positionTime)
                    return .success
                }
                return .commandFailed
            }

            commandCenter.nextTrackCommand.addTarget { [unowned self] event in
                self.playerSkipForward()
                return .success
            }

            commandCenter.previousTrackCommand.addTarget { [unowned self] event in
                self.playerSkipBack()
                return .success
            }

            commandCenterInitialised = true
        }
    }

    private func setNowPlayingInfoCenter() {
//        nowPlayingInfo = [String : Any]()

        if let trackItem = playlist[currentIndex] {
            nowPlayingInfo[MPMediaItemPropertyTitle] = trackItem["title"] ?? ""
            nowPlayingInfo[MPMediaItemPropertyAlbumTitle] = trackItem["album"] ?? ""
            nowPlayingInfo[MPMediaItemPropertyArtist] = trackItem["artist"] ?? ""

            nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackRate] = 0
            nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackQueueIndex] = currentIndex
            nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackQueueCount] = playlist.count

            if trackItem["isStream"] != nil && trackItem["isStream"] as! Bool {
                nowPlayingInfo.removeValue(forKey: "MPMediaItemPropertyPlaybackDuration")
                nowPlayingInfo.removeValue(forKey: "MPNowPlayingInfoPropertyElapsedPlaybackTime")
                nowPlayingInfo[MPNowPlayingInfoPropertyIsLiveStream] = 1.0
            } else {
                nowPlayingInfo[MPMediaItemPropertyPlaybackDuration] = NSNumber.init(value : CMTimeGetSeconds(player.currentItem!.duration))
                nowPlayingInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = NSNumber.init(value : CMTimeGetSeconds(player.currentTime()))
                nowPlayingInfo.removeValue(forKey: "MPNowPlayingInfoPropertyIsLiveStream")
            }

            MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo

            if let imageUrl = trackItem["coverImage"] {
                if let url = URL( string:imageUrl as! String)
                {
                    DispatchQueue.global().async {
                      if let data = try? Data( contentsOf:url)
                      {
                        DispatchQueue.main.async {
                            let coverImage = self.cropToBounds(image: UIImage( data:data)!, width: 100, height: 100)
                            self.nowPlayingInfo[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(boundsSize: coverImage.size, requestHandler: { (size) -> UIImage in return coverImage})
                            MPNowPlayingInfoCenter.default().nowPlayingInfo = self.nowPlayingInfo
                        }
                      } else {
                        self.nowPlayingInfo.removeValue(forKey: "MPMediaItemPropertyArtwork")
                      }
                   }
                } else {
                    nowPlayingInfo.removeValue(forKey: "MPMediaItemPropertyArtwork")
                }
            } else {
                nowPlayingInfo.removeValue(forKey: "MPMediaItemPropertyArtwork")
            }
        }
    }

    private func updateNowPlayingInfoCenter() {
        if let trackItem = playlist[currentIndex] {
            nowPlayingInfo[MPMediaItemPropertyTitle] = trackItem["title"] ?? ""
            nowPlayingInfo[MPMediaItemPropertyAlbumTitle] = trackItem["album"] ?? ""
            nowPlayingInfo[MPMediaItemPropertyArtist] = trackItem["artist"] ?? ""

            nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackRate] = player.rate
            nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackQueueIndex] = currentIndex
            nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackQueueCount] = playlist.count

            if trackItem["isStream"] != nil && trackItem["isStream"] as! Bool {
                nowPlayingInfo.removeValue(forKey: "MPMediaItemPropertyPlaybackDuration")
                nowPlayingInfo.removeValue(forKey: "MPNowPlayingInfoPropertyElapsedPlaybackTime")
                nowPlayingInfo[MPNowPlayingInfoPropertyIsLiveStream] = 1.0
            } else {
                nowPlayingInfo[MPMediaItemPropertyPlaybackDuration] = NSNumber.init(value : CMTimeGetSeconds(player.currentItem!.duration))
                nowPlayingInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = NSNumber.init(value : CMTimeGetSeconds(player.currentTime()))
                nowPlayingInfo.removeValue(forKey: "MPNowPlayingInfoPropertyIsLiveStream")
            }


            MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
        }
    }

    private func clearNowPlayingInfoCenter() {
        nowPlayingInfo = [String : Any]()
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
    }


    /**
    *
    * Player actions
    * These are the public API for the player and wrap most of the complexity of the queue.
    *
    *
    */

    private func playerSetItems(items: JSArray, index: Int) {
        resetPlayer()

        playlist = items
        currentIndex = index

        loadPlayerItem(isAuto: false)
    }

    private func playerSetAutoPlaylistItems(items: JSArray) {
        print("set auto playlist items \(items.count)")
        autoPlaylist = items
        autoPlaylistIndex = 0
    }

    private func playerAddItems(items: JSArray) {
        let needsLoad = playlist.count < 1 || playlist[currentIndex] == nil;
        playlist.append(contentsOf: items)

        DispatchQueue.main.async {
            self.setupNowPlayingInfoCenter()
        }

        if needsLoad {
            resetPlayer()
            loadPlayerItem(isAuto: false)
        }
    }

    private func playerClearAllItems() {
        resetPlayer()

        playlist.removeAll()
        currentIndex = 0
        autoPlaylist.removeAll()
        autoPlaylistIndex = 0

        clearNowPlayingInfoCenter()
        sendSyncEvent(type: AudioPlayerStatus.GENERAL)
    }

    private func playerRemoveItem(index: Int) {
        let isPlaying:Bool = currentState == "playing"
        let removedCurrent:Bool = index == currentIndex

        if removedCurrent && isPlaying {
            resetPlayer()
        }

        playlist.remove(at: index)

        if removedCurrent {
            if playlist[currentIndex] != nil {
                if isPlaying {
                    playerPlayByIndex(index: currentIndex)
                }
            } else if let autoItem = getAutoPlayItem(index: autoPlaylistIndex) {
                playlist.append(autoItem)
                sendSyncEvent(type: AudioPlayerStatus.PLAYLIST_ADD_ITEM)

                loadPlayerItem(isAuto: true)

                if isPlaying {
                    player.play()
//                    do {
//                        try audioSession.setActive(true)
//                    } catch {
//
//                    }
                    updateNowPlayingInfoCenter()
                    currentState = "playing"
                    sendSyncEvent(type: AudioPlayerStatus.GENERAL)
                }
            }
        }
    }

    private func playerSetUseAutoPlaylist(use: Bool) {
        useAutoPlaylist = use
        sendSyncEvent(type: AudioPlayerStatus.GENERAL)
    }

    private func playerClearAutoPlaylistItems() {
        autoPlaylist.removeAll()
        autoPlaylistIndex = 0
        sendSyncEvent(type: AudioPlayerStatus.GENERAL)
    }

    private func playerPlay() {
        if player.timeControlStatus == AVPlayer.TimeControlStatus.paused {
            player.play()
//            do {
//                try audioSession.setActive(true)
//            } catch {
//
//            }
            updateNowPlayingInfoCenter()
            currentState = "playing"
            sendSyncEvent(type: AudioPlayerStatus.GENERAL)
        }
    }

    private func playerPlayByIndex(index: Int) {
        guard playlist.indices.contains(index) else { return }
        resetPlayer()

        currentIndex = index

        loadPlayerItem(isAuto: false)

        player.play()
//        do {
//            try audioSession.setActive(true)
//        } catch {
//
//        }
        updateNowPlayingInfoCenter()
        currentState = "playing"
        sendSyncEvent(type: AudioPlayerStatus.GENERAL)
    }

    private func playerPlayAutoByIndex(index: Int) {
        guard autoPlaylist.indices.contains(index) else { return }
        resetPlayer()

        playlist.append(autoPlaylist[index])
        sendSyncEvent(type: AudioPlayerStatus.PLAYLIST_ADD_ITEM)

        currentIndex = playlist.count - 1

        loadPlayerItem(isAuto: false)

        player.play()
//        do {
//            try audioSession.setActive(true)
//        } catch {
//
//        }
        updateNowPlayingInfoCenter()
        currentState = "playing"
        sendSyncEvent(type: AudioPlayerStatus.GENERAL)
    }

    private func playerPause() {
        if player.timeControlStatus == AVPlayer.TimeControlStatus.playing {
            player.pause()
            updateNowPlayingInfoCenter()
            currentState = "paused"
            sendSyncEvent(type: AudioPlayerStatus.GENERAL)
        }
    }

    private func playerInteruptionPause() {
        if player.timeControlStatus == AVPlayer.TimeControlStatus.playing {
            player.pause()
            updateNowPlayingInfoCenter()
        }

        currentState = "paused"
        sendSyncEvent(type: AudioPlayerStatus.GENERAL)
    }

    private func playerSkipForward() {
        let index = currentIndex + 1
        if playlist.indices.contains(index) {
            currentIndex = index
            loadPlayerItem(isAuto: false)
            player.play()
//            do {
//                try audioSession.setActive(true)
//            } catch {
//
//            }
            updateNowPlayingInfoCenter()
            currentState = "playing"
            sendSyncEvent(type: AudioPlayerStatus.GENERAL)
        } else if let autoItem = getAutoPlayItem(index: autoPlaylistIndex) {
            playlist.append(autoItem)
            currentIndex = index
            sendSyncEvent(type: AudioPlayerStatus.PLAYLIST_ADD_ITEM)

            loadPlayerItem(isAuto: true)
            player.play()
//            do {
//                try audioSession.setActive(true)
//            } catch {
//
//            }
            updateNowPlayingInfoCenter()
            currentState = "playing"
            sendSyncEvent(type: AudioPlayerStatus.GENERAL)
        } else {
            player.pause()
            player.seek(to: CMTime.init(seconds: 0, preferredTimescale: CMTimeScale(NSEC_PER_SEC)))
            currentState = "paused"
            currentPosition = 0
            updateNowPlayingInfoCenter()
            sendSyncEvent(type: AudioPlayerStatus.GENERAL)
        }
    }

    private func playerSkipBack() {
        let index = currentIndex - 1
        print("new index - \(index)")

        if playlist.indices.contains(index) {
            currentIndex = index
            loadPlayerItem(isAuto: false)
            player.play()
//            do {
//                try audioSession.setActive(true)
//            } catch {
//
//            }
            updateNowPlayingInfoCenter()
            currentState = "playing"
            sendSyncEvent(type: AudioPlayerStatus.GENERAL)
        } else {
            player.pause()
            player.seek(to: CMTime.init(seconds: 0, preferredTimescale: CMTimeScale(NSEC_PER_SEC)))
            currentState = "paused"
            currentPosition = 0
            updateNowPlayingInfoCenter()
            sendSyncEvent(type: AudioPlayerStatus.GENERAL)
        }
    }

    private func playerSeekTo(position: TimeInterval) {
        print("seek to \(Float(position))")
        isSeeking = true
        player.seek(to: CMTime(seconds: position, preferredTimescale: CMTimeScale(1000)))
        currentPosition = Float(player.currentTime().seconds)
        updateNowPlayingInfoCenter()
        sendSyncEvent(type: AudioPlayerStatus.GENERAL)
    }

    private func playerSetPlaybackRate(rate: Float) {
        print("set playback rate to \(rate)")
        player.setRate(rate, time: CMTime.invalid, atHostTime: CMTime.invalid)
        sendSyncEvent(type: AudioPlayerStatus.GENERAL)
    }

    private func playerSetVolume(volume: Float) {
        print("set volume to \(volume)")
        player.volume = volume
        sendSyncEvent(type: AudioPlayerStatus.GENERAL)
    }

    private func playerReset() {
        if _playerEndObserver != nil {
            NotificationCenter.default.removeObserver(_playerEndObserver)
        }
    }





    /**
    *
    * Capacitor Interface
    * These are basically just passing through to the core functionality of this player.
    * These functions don't really do anything interesting by themselves.
    *
    */

    @objc func initialise(_ call: CAPPluginCall) {
        if(initialised == false) {
            // play in background
            do {
                try self.audioSession.setCategory(AVAudioSession.Category.playback)
                try audioSession.setActive(true)

                _playerEndObserver = NotificationCenter.default.addObserver(self, selector: #selector(self.trackDidEnd), name:NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
                _playerInteruptionObserver = NotificationCenter.default.addObserver(self, selector: #selector(self.interruptionHandler), name:AVAudioSession.interruptionNotification, object: nil)

                self.initialised = true
                call.success()
            } catch {
                call.reject("Initialise failed")
            }
        }
    }

    @objc func setItems(_ call: CAPPluginCall) {
        let items = call.getArray("items", [String:Any].self) ?? []
        let index = call.getInt("index")

        playerSetItems(items: items, index: index ?? 0)

        call.success(["playlist": self.playlist, "currentIndex": self.currentIndex])
    }

    @objc func addItems(_ call: CAPPluginCall) {
        let items = call.getArray("items", [String:Any].self) ?? []

        playerAddItems(items: items)

        call.success(["playlist": self.playlist, "currentIndex": self.currentIndex])
    }

    @objc func setAutoPlaylistItems(_ call: CAPPluginCall) {
        let items = call.getArray("items", [String:Any].self) ?? []

        playerSetAutoPlaylistItems(items: items)

        call.success(["autoPlaylist": self.autoPlaylist, "autoPlaylistIndex": self.autoPlaylistIndex])
    }

    @objc func clearAllItems(_ call: CAPPluginCall) {
        playerClearAllItems()
        call.success()
    }

    @objc func removeItem(_ call: CAPPluginCall) {
        let index = call.getInt("index") ?? 0

        playerRemoveItem(index: index)

        call.success()
    }

    @objc func setUseAutoPlaylist(_ call: CAPPluginCall) {
        guard let use = call.getBool("use") else { return call.reject("need a use value") }
        playerSetUseAutoPlaylist(use: use)
        call.success()
    }

    @objc func clearAutoPlaylistItems(_ call: CAPPluginCall) {
        playerClearAutoPlaylistItems()
        call.success()
    }

    @objc func play(_ call: CAPPluginCall) {
        playerPlay()
        call.success()
    }

    @objc func playByIndex(_ call: CAPPluginCall) {
        let index = call.getInt("index") ?? -1
        playerPlayByIndex(index: index)
        call.success()
    }

    @objc func playAutoByIndex(_ call: CAPPluginCall) {
        let index = call.getInt("index") ?? -1
        playerPlayAutoByIndex(index: index)
        call.success()
    }

    @objc func pause(_ call: CAPPluginCall) {
        playerPause()
        call.success()
    }

    @objc func skipForward(_ call: CAPPluginCall) {
        playerSkipForward()
        call.success()
    }

    @objc func skipBack(_ call: CAPPluginCall) {
        playerSkipBack()
        call.success()
    }

    @objc func seekTo(_ call: CAPPluginCall) {
        guard let position = call.getDouble("position") else { return call.reject("need a position") }
        playerSeekTo(position: position)
        call.success()
    }

    @objc func setPlaybackRate(_ call: CAPPluginCall) {
        guard let rate = call.getFloat("rate") else { return call.reject("need a rate") }
        playerSetPlaybackRate(rate: rate)
        call.success()
    }

    @objc func setVolume(_ call: CAPPluginCall) {
        guard let volume = call.getFloat("volume") else { return call.reject("need a volume") }
        playerSetVolume(volume: volume)
        call.success()
    }

    @objc func setMuted(_ call: CAPPluginCall) {
//        guard let muted = call.getBool("muted") else { return call.reject("need a mute value") }
//        playerSetMuted(muted: muted)
        call.success()
    }

    @objc func reset(_ call: CAPPluginCall) {
        playerReset()
        call.success()
    }
}
