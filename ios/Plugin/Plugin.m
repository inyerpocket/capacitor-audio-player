#import <Foundation/Foundation.h>
#import <Capacitor/Capacitor.h>

// Define the plugin using the CAP_PLUGIN Macro, and
// each method the plugin supports using the CAP_PLUGIN_METHOD macro.
CAP_PLUGIN(AudioPlayerPlugin, "AudioPlayerPlugin",
          CAP_PLUGIN_METHOD(initialise, CAPPluginReturnPromise);
          CAP_PLUGIN_METHOD(setItems, CAPPluginReturnPromise);
          CAP_PLUGIN_METHOD(setAutoPlaylistItems, CAPPluginReturnPromise);
          CAP_PLUGIN_METHOD(addItems, CAPPluginReturnPromise);
          CAP_PLUGIN_METHOD(clearAllItems, CAPPluginReturnPromise);
          CAP_PLUGIN_METHOD(removeItem, CAPPluginReturnPromise);
          CAP_PLUGIN_METHOD(setUseAutoPlaylist, CAPPluginReturnPromise);
          CAP_PLUGIN_METHOD(clearAutoPlaylistItems, CAPPluginReturnPromise);
          CAP_PLUGIN_METHOD(play, CAPPluginReturnPromise);
          CAP_PLUGIN_METHOD(playByIndex, CAPPluginReturnPromise);
          CAP_PLUGIN_METHOD(playAutoByIndex, CAPPluginReturnPromise);
          CAP_PLUGIN_METHOD(pause, CAPPluginReturnPromise);
          CAP_PLUGIN_METHOD(skipForward, CAPPluginReturnPromise);
          CAP_PLUGIN_METHOD(skipBack, CAPPluginReturnPromise);
          CAP_PLUGIN_METHOD(seekTo, CAPPluginReturnPromise);
          CAP_PLUGIN_METHOD(setPlaybackRate, CAPPluginReturnPromise);
          CAP_PLUGIN_METHOD(setVolume, CAPPluginReturnPromise);
          CAP_PLUGIN_METHOD(setMuted, CAPPluginReturnPromise);
          CAP_PLUGIN_METHOD(reset, CAPPluginReturnPromise);
)
