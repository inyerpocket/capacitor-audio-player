enum AudioPlayerStatus {
    static let GENERAL = 1

    static let TRACK = 10
    static let POSITION = 11

    static let PLAYLIST = 20
    static let PLAYLIST_AUTO = 21
    static let PLAYLIST_TRACK_CHANGE = 22
    static let PLAYLIST_ADD_ITEM = 23
}
