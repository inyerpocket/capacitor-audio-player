declare module '@capacitor/core' {
  interface PluginRegistry {
    AudioPlayerPlugin: AudioPlayerPluginPlugin;
  }
}

export interface AudioPlayerPluginPlugin {
  initialise(): Promise<void>;
  setItems(options: ItemsOptions): Promise<PlaylistResultData>;
  setAutoPlaylistItems(options: ItemsOptions): Promise<AutoPlaylistResultData>;
  addItems(options: ItemsOptions): Promise<PlaylistResultData>;
  clearAllItems(): Promise<PlaylistResultData>;
  removeItem(options: ItemOptions): Promise<PlaylistResultData>;
  setUseAutoPlaylist(options: UseAutoPlaylistOptions): Promise<any>;
  clearAutoPlaylistItems(): Promise<any>;
  play(): Promise<any>;
  playByIndex(options: ItemOptions): Promise<any>;
  playAutoByIndex(options: ItemOptions): Promise<any>;
  pause(): Promise<any>;
  skipForward(): Promise<any>;
  skipBack(): Promise<any>;
  seekTo(options: SeekOptions): Promise<any>;
  setPlaybackRate(options: PlaybackRateOptions): Promise<any>;
  setVolume(options: VolumeOptions): Promise<any>;
  setMuted(options: MuteOptions): Promise<any>;
  reset(): Promise<any>;
}

export interface TrackItem {
  id: string;
  title: string;
  artist: string;
  album: string;
  coverImage: string;
  assetUrl: string;
  mediaType?: string;
  isStream: boolean;
  isLocal: boolean;
  playStatUrl?: string;
  autoPlayStatUrl?: string;
}

export interface ItemsOptions {
  items: TrackItem[];
}

export interface ItemOptions {
  index: number;
}

export interface UseAutoPlaylistOptions {
  use: boolean;
}

export interface SeekOptions {
  position: number;
}

export interface PlaybackRateOptions {
  rate: float;
}

export interface VolumeOptions {
  volume: float;
}

export interface MuteOptions {
  mute: boolean;
}

export interface PlaylistResultData {
  playlist: TrackItem[];
  currentIndex: number;
}

export interface AutoPlaylistResultData {
  autoPlaylist: TrackItem[];
  autoPlaylistIndex: number;
}
