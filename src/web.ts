import { WebPlugin } from '@capacitor/core';
import { AudioPlayerPluginPlugin, TrackItem, ItemsOptions, ItemOptions, UseAutoPlaylistOptions, SeekOptions, PlaybackRateOptions, VolumeOptions, MuteOptions, PlaylistResultData, AutoPlaylistResultData } from './definitions';

export class AudioPlayerPluginWeb extends WebPlugin implements AudioPlayerPluginPlugin {
  constructor() {
    super({
      name: 'AudioPlayerPlugin',
      platforms: ['web'],
    });
  }

  async initialise(): Promise<void> {
    console.log('initialise', 'not implemented');
    return;
  }

  async setItems(options: ItemsOptions): Promise<PlaylistResultData> {
    console.log('setItems', 'not implemented');
    return;
  }

  async setAutoPlaylistItems(options: ItemsOptions): Promise<AutoPlaylistResultData> {
    console.log('setAutoPlaylistItems', 'not implemented');
    return;
  }

  async addItems(options: ItemsOptions): Promise<PlaylistResultData> {
    console.log('addItems', 'not implemented');
    return;
  }

  async clearAllItems(): Promise<PlaylistResultData> {
    console.log('clearAllItems', 'not implemented');
    return;
  }

  async removeItem(options: ItemOptions): Promise<PlaylistResultData> {
    console.log('removeItem', 'not implemented');
    return;
  }

  async setUseAutoPlaylist(options: UseAutoPlaylistOptions): Promise<any> {
    console.log('setUseAutoPlaylist', 'not implemented');
    return;
  }

  async clearAutoPlaylistItems(): Promise<any> {
    console.log('clearAutoPlaylistItems', 'not implemented');
    return;
  }

  async play(): Promise<any> {
    console.log('play', 'not implemented');
    return;
  }

  async playByIndex(options: ItemOptions): Promise<any> {
    console.log('playByIndex', 'not implemented');
    return;
  }

  async playAutoByIndex(options: ItemOptions): Promise<any> {
    console.log('playAutoByIndex', 'not implemented');
    return;
  }

  async pause(): Promise<any> {
    console.log('pause', 'not implemented');
    return;
  }

  async skipForward(): Promise<any> {
    console.log('pause', 'not implemented');
    return;
  }

  async skipBack(): Promise<any> {
    console.log('pause', 'not implemented');
    return;
  }

  async seekTo(options: SeekOptions): Promise<any> {
    console.log('pause', 'not implemented');
    return;
  }

  async setPlaybackRate(options: PlaybackRateOptions): Promise<any> {
    console.log('setPlaybackRate', 'not implemented');
    return;
  }

  async setVolume(options: VolumeOptions): Promise<any> {
    console.log('setVolume', 'not implemented');
    return;
  }

  async setMuted(options: MuteOptions): Promise<any> {
    console.log('setMuted', 'not implemented');
    return;
  }

  async reset(): Promise<any> {
    console.log('reset', 'not implemented');
    return;
  }
}

const AudioPlayerPlugin = new AudioPlayerPluginWeb();

export { AudioPlayerPlugin };

import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(AudioPlayerPlugin);
